import { supabase } from "$lib/supabaseClient";


/** @type {import('./$types').LayoutServerLoad} */
export async function load({ locals, request }) {
    try
    {
        const photo  = await supabase.from("Images").select().eq("user_id" , locals.session.data.user.id);
        return {
            session: locals.session.data, //объект cookie-session
            layout_custom_data: 999,
            photo : photo.data[0].byteimage? photo.data[0].byteimage : null
        }
    }
    catch
    {
        return {
            session: locals.session.data,
            layout_custom_data: 999}
    }

};

