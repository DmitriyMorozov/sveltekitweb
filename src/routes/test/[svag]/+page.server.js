/** @type {import('./$types').PageServerLoad} */
export async function load({ url, cookies, request, locals }) {
	let cook_adder = cookies.get('adder')
	let text = cookies.get("textarea")
	if (!cook_adder) {
		cookies.set("adder", "0")
	} else {
		if (!new URL(request.url).search) {
			cookies.set("adder", "0")
			return { summa: 0 }
		}
		return { summa: parseInt(cook_adder), txt: text }
	}
}


/**@type {import('./$types').Actions} */
export const actions = {
	create: async ({ locals, cookies, url, request, params }) => {
		const data = await request.formData()
		let answer = data.get("description")
		let text = data.get("textarea")
		let summ = url.searchParams.get('/create')

		let cook_adder = cookies.get('adder')
		cookies.set('adder', (parseInt(answer) + parseInt(cook_adder)).toString(10))

		
		cookies.set('txt', text)

		let res = parseInt(answer) + parseInt(summ);

		if (parseInt(answer) == 100) {
            const { data1 = [] } = locals.session.data;
            if (data1.length == 0) {
                await locals.session.set({ views: 222, data1: [-321], data2: [] });
            } else {
                await locals.session.update((data) => ({ data1: [...data.data1, 5, 6, 7] }))
            }
        }

		return { summ: res, txt: text }
	}
};