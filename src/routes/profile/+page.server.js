import { fail, redirect } from '@sveltejs/kit';
import { supabase } from "$lib/supabaseClient";
import bcrypt from 'bcrypt'


export async function load({ locals }) {
	if (locals.stop) {
		throw redirect(303, '/login')
	}
	else {
		const photo = await supabase.from("Images").select().eq("user_id", locals.session.data.user.id);
		const { data } = await supabase.from("Users").select().eq("id", locals.session.data.user.id);
		return {
			name: data[0].name,
			mail: data[0].username,
			photo: photo.data[0].byteimage ? photo.data[0].byteimage : null
		}
	}
}


export const actions = {
	update: async ({ request, locals }) => {
		const datas = await request.formData();
		let blob = datas.get("b64");
		try {
			const { data, error } = await supabase
				.from('Images')
				.update([
					{ byteimage: blob },
				])
				.eq("user_id", locals.session.data.user.id)
		} catch { }

		try {
			let name = datas.get("login");
			let mail = datas.get("email");
			const { data, error } = await supabase
				.from('Users')
				.update([
					{ name: name, username: mail, updated_at: new Date()  },
				])
				.eq("id", locals.session.data.user.id)
		} catch { }

		try {
			let oldpassword = datas.get("oldpassword");
			let password = datas.get("newpassword");
			if (oldpassword != password) {
				const passwordHash = await supabase.from("Users").select().eq("id", locals.session.data.user.id);
				const checkPassword = await bcrypt.compare(oldpassword, passwordHash.data[0].passwordHash)
				if(checkPassword)
				{
					const { data, error } = await supabase
					.from('Users')
					.update([
						{ passwordHash: await bcrypt.hash(password, 10), updated_at: new Date()  },
					])
					.eq("id", locals.session.data.user.id)
				}
				else
				{
					 return fail(400, { bad_pwd: true ,value: "Неверный пароль"})
				}	
			}
			else
			{
				 return fail(400, { bad_pwd: true ,value: "Пароли совпадают"})
			}

		} catch { }
		return fail(303, { good: true})
		// throw redirect(303, '/profile')		
	}
};