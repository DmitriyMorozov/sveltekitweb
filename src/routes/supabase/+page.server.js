import { supabase } from "$lib/supabaseClient";
import { fail, redirect } from '@sveltejs/kit';

export async function load({ locals  }) {
  
  if (locals.stop)
  {
    throw redirect(303, '/login')
  }
  else
  {
    const { data } = await supabase.from("countries").select();
    return {
    countries: data ?? [],
    };
  }
  
  
  
}


/**@type {import('./$types').Actions} */
export const actions = {
  insert_country: async ({ request }) => {
    const data = await request.formData()

    let namecountry = data.get("names")

    const { error } = await supabase
      .from('countries')
      .insert([
        { name: namecountry },
      ]);


    if (error) {
      return fail(parseInt(error.code), { error: error.message });
    }
  },
  delete_country: async ({ locals, cookies, url, request }) => {
    const data = await request.formData()

    let ids = data.get("ids")
    
    const { error } = await supabase
      .from('countries')
      .delete()
      .eq('id',ids)

    if (error) {
      return fail(parseInt(error.code), { error: error.message });
    }
  }
}
