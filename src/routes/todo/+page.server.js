import * as db from '$lib/database.js';
import { fail, redirect } from '@sveltejs/kit';
import { supabase } from "$lib/supabaseClient";
import * as fs from 'fs'

export async function load({ locals }) {
	if (locals.stop) {
		throw redirect(303, '/login')
	}
	else {
		const role = await supabase.from("Users").select().eq("roles", '2');
		let flag = false;
		for (let i = 0; i < Number(role.data?.length); i++) {
			if (role) {
				if (role.data[i].id == locals.session.data.user.id) {
					flag = true;
					break;
				}
				else {
					flag = false;

				}
			}

		}
		const { data } = await supabase.from("ToDo").select().eq("user_id", locals.session.data.user.id);
		// const glob_import = import.meta.glob("/static/*" ,{ eager: true });
		// const projects = Object.entries(glob_import);
		
		let folder = 'static'
		let projects = fs.readdirSync(folder)
		// console.log(projects)

		if (flag == true) {
			const users = await supabase.from("Users").select().eq("roles", '1');
			// console.log(users)
			return {
				files: projects,
				todos: data,
				admin: flag,
				users: users.data
			};
		}
		else {
			return {
				files: projects,
				todos: data,
				admin: flag,
				users: null
			}
		};
	}
}

export const actions = {
	create: async ({ cookies, request }) => {
		const data = await request.formData();
		try {
			db.createTodo(cookies.get('userid'), data.get('description'), data.get('date'));
			return { resault: true }
		} catch (error) {
			return fail(422, {
				description: data.get('description'),
				error: error.message
			});
		}
	},
	delete: async ({ cookies, request }) => {
		const data = await request.formData();
		db.deleteTodo(cookies.get('userid'), data.get('id'));
	},
	deletebase: async ({ request }) => {
		const data = await request.formData();
		console.log(data.get("id"))
		if (!data.admin) {
			const new_user = await supabase
				.from('ToDo')
				.delete()
				.eq("id", data.get("id"))
		}
		const file =  data.get('file');
		console.log(file)
		if (file)
		{
			fs.unlink(`static/${file}`,(err) => {
			if (err) {
				return fail(500, { error: err.message });
			}
			console.log('Файл Удален');
		})
		}
	
	},
	createbase: async ({ request, locals }) => {
		const data = await request.formData();
		const file =  data.get('file');
		if (file.size !=0) {
			const filedata = new Uint8Array(Buffer.from(await file.arrayBuffer()));
			fs.writeFile(`static/${file.name}`, filedata, (err) => {
				if (err) {
					return fail(500, { error: err.message });
				}
				console.log('Файл сохранен');
			});
		}
		const new_todo = await supabase
			.from('ToDo')
			.insert(
				{
					value: data.get('description'),
					user_id: locals.session.data.user.id,
					date: data.get('date'),
					file: file.name
				}
			)


	

	},
	getfile: async () => {
		const imageModules = import.meta.glob("/static/*.*");
		console.log(imageModules);
		return {
			files: imageModules,
		};
		//   for (const modulePath in imageModules) {
		//     imageModules[modulePath]().then(({ default: imageUrl }) => {
		//       console.log(modulePath, imageUrl);
		//     });
		//   }
	},
	loadfile: async () => {
		const imageModules = import.meta.glob("/static/*.*");
		console.log(imageModules);
		//   for (const modulePath in imageModules) {
		//     imageModules[modulePath]().then(({ default: imageUrl }) => {
		//       console.log(modulePath, imageUrl);
		//     });
		//   }
	},
};
