import { supabase } from "$lib/supabaseClient";
import { SESSIONNAME } from '$env/static/private';
import { json, type RequestHandler, redirect, fail } from '@sveltejs/kit';

export const GET: RequestHandler = async ({ url }) => {

    let id = url.searchParams.get('id')
    const { data } = await supabase
        .from('ToDo')
        .select()
        .eq('user_id', id)

    // console.log("error", data)

    return json({ data }, { status: 201 });
    //https://learn.svelte.dev/tutorial/get-handlers
};

