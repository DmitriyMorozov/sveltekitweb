export const posts = [
	{
		slug: 'meat',
		title: 'Welcome to the Aperture Science computer-aided enrichment center',
		content:
			'<p>We hope your brief detention in the relaxation vault has been a pleasant one.</p><p>Your specimen has been processed and we are now ready to begin the test proper.</p>'
	},

	{
		slug: 'milk',
		title: 'Safety notice',
		content:
			'<p>While safety is one of many Enrichment Center Goals, the Aperture Science High Energy Pellet, seen to the left of the chamber, can and has caused permanent disabilities, such as vaporization. Please be careful.</p>'
	}
];